#!/usr/lib/python3
from typing import Generator
from pathlib import Path


def output_data_from_huge_files(*paths_to_files: str) \
        -> Generator[str, None, None]:
    """
    A eternal generator that output lines from files in a
    multiplexed file-by-file line-by-line order

    :param paths_to_files: paths to files in str format
    :return: Generator
    """

    for file_path in paths_to_files:
        assert isinstance(file_path, str), 'Paths must be string'

    file_paths_objects = ([
        Path(file_path).absolute() for file_path in paths_to_files
    ])
    for file_path in file_paths_objects:
        assert file_path.exists(), f'File doesnt exists: {str(file_path)}'
        assert file_path.is_file(), f'Its not a file: {str(file_path)}'

    file_descriptors = ([
        file_path.open(mode='r') for file_path in file_paths_objects
    ])

    while True:
        for file_descriptor in file_descriptors:
            line = file_descriptor.readline()

            if not line:
                file_descriptor.seek(0)
                line = file_descriptor.readline()

            yield line
