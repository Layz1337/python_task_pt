#!/usr/lib/python3
import re

TIME_PATTER = re.compile(r'^(?P<numbers>\d+(?:\.?\d+)?)*(?P<letter>\w*)$')
WORDS_MAP = {
    's': 1,
    'm': 60,
    'h': 60 * 60,
    'd': 60 * 60 * 60,
}


def get_timedelta_from_str(str_time: str) -> int:
    """
    A function that converts a time in string format to integer format

    :param str_time: time in string format
    :return: integer time representation in seconds
    """

    assert str_time, 'str_time cannot be empty'
    assert isinstance(str_time, str), 'str_time must be string'

    search_result = TIME_PATTER.search(str_time)
    assert search_result, 'Wrong time format'

    numbers, letter = search_result['numbers'], search_result['letter']

    if not numbers:
        numbers = 1
    if not letter:
        letter = 's'

    assert letter in WORDS_MAP.keys(), 'Wrong time format'

    return int(
        float(numbers) * WORDS_MAP[letter]
    )

