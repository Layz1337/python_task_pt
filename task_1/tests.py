import pytest
from get_time_delta_in_seconds import get_timedelta_from_str


@pytest.mark.parametrize(
    'test_data, expected_result',
    [
        (
            '30',
            30
        ),
        (
            '30s',
            30
        ),
        (
            's',
            1
        ),
        (
            '60.5m',
            3630
        ),
        (
            '1h',
            60 * 60
        ),
        (
            '0',
            0
        ),
    ],
    ids=[
        'example_only_numbers_30_test_case',
        'example_number_and_letter_30s_test_case',
        'example_only_letter_s_test_case',
        'example_number_with_dot_and_letter_60.5m_test_case',
        '1h_test_case',
        '0_test_case',
    ],
)
def test_ok_time_converting(test_data, expected_result):
    result = get_timedelta_from_str(test_data)
    assert result == expected_result


@pytest.mark.parametrize(
    'test_data, expected_error',
    [
        (
            '10seconds',
            {
                'expected_exception': AssertionError,
                'match': 'Wrong time format',
            },
        ),
        (
            '1y',
            {
                'expected_exception': AssertionError,
                'match': 'Wrong time format',
            },
        ),
        (
            '',
            {
                'expected_exception': AssertionError,
                'match': 'str_time cannot be empty',
            },
        ),
        (
            1,
            {
                'expected_exception': AssertionError,
                'match': 'str_time must be string',
            },
        ),
        (
            '3000smmmm',
            {
                'expected_exception': AssertionError,
                'match': 'Wrong time format',
            },
        ),
        (
            '300.3.3m',
            {
                'expected_exception': AssertionError,
                'match': 'Wrong time format',
            },
        ),
        (
            None,
            {
                'expected_exception': AssertionError,
                'match': 'str_time cannot be empty',
            },
        ),
        (
            '60,5h',
            {
                'expected_exception': AssertionError,
                'match': 'Wrong time format',
            },
        ),
    ],
    ids=[
        'example_10seconds',
        'example_1y',
        'example_empty_time_string',
        'time_not_str',
        'wrong_time_format_1',
        'wrong_time_format_2',
        'wrong_type_none',
        'wrong_numbers_separator_comma'
    ],
)
def test_not_ok_time_converting(test_data, expected_error):
    with pytest.raises(**expected_error):
        result = get_timedelta_from_str(test_data)
