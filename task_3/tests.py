import pytest
from merge_function import merge


@pytest.mark.parametrize(
    'test_data, expected_result',
    [
        (
            (
                iter((1, 1, 2, 3, 4)),
                iter((1, 3, 3, 4)),
                iter((1, 2, 3, 3, 4)),
                iter((1, 2, 3, 3, 4)),
                iter((6, 8, 10)),
                iter((15, 18, 100, 122, 199))

            ),
            [
                1, 1, 1, 1, 1, 2, 2, 2, 3, 3, 3, 3, 3, 3, 3,
                4, 4, 4, 4, 6, 8, 10, 15, 18, 100, 122, 199
            ],
        ),
        (
            (
                iter((1, 5, 9)),
                iter((2, 5)),
                iter((1, 6, 10, 11)),
            ),
            [
                1, 1, 2, 5, 5, 6, 9, 10, 11
            ],
        ),
        (
            (),
            [],
        ),
        (
            (
                iter((1, 5, 9)),
                iter(()),
                iter((1, 6, 10, 11)),
            ),
            [
                1, 1, 5, 6, 9, 10, 11
            ],
        ),
        (
            (
                iter(()),
            ),
            [],
        ),
    ],
    ids=[
        'my_own_test_case',
        'example_test_case',
        'empty_input_test_case',
        'one_of_iterators_are_empty',
        'only_one_iterator_and_its_empty'
    ],
)
def test_ok_merge(test_data, expected_result):
    result = [item for item in merge(*test_data)]
    assert result == expected_result


@pytest.mark.parametrize(
    'test_data, expected_error',
    [
        (
            ('its_not_iterator',),
            {
                'expected_exception': AssertionError,
                'match': f'Passed parameter must be an iterator, got {str}',
            },
        ),
        (
            (
                iter(('1', 1, 2, 3, 4)),
                iter((1, 3, 3, 4)),
            ),
            {
                'expected_exception': AssertionError,
                'match': f'Values must be integer, but {"1"!r} its {str}',
            },
        ),
        (
            (
                iter((1, 1, '2', 3, 4)),
                iter((1, 3, 3, 4)),
            ),
            {
                'expected_exception': AssertionError,
                'match': f'Values must be integer, but {"2"!r} its {str}',
            },
        ),
        (
            (
                    iter((1, 3, 2, 3, 4)),
                    iter((1, 5, 3, 4)),
            ),
            {
                'expected_exception': AssertionError,
                'match': f'Iterators values arent sorted',
            },
        ),
    ],
    ids=[
        'input_not_iterator',
        'iterator_value_not_integer_when_the_buffer_is_first_filled',
        'iterator_value_not_integer',
        'iterator_values_not_sorted',
    ],
)
def test_not_ok_merge(test_data, expected_error):
    with pytest.raises(**expected_error):
        result = [item for item in merge(*test_data)]
