#!/usr/lib/python3
from collections.abc import Iterator
from typing import Generator


def merge(*iterators: Iterator) -> Generator[int, None, None]:
    """
    A generator that implements output of
    sorted values from iterators (from minimum to maximum)

    :param iterators: iterators, values must be sorted from smallest to largest
    :return: Generator
    """

    buffer = list()
    for index_of_iterable, iterable in enumerate(iterators):
        assert isinstance(iterable, Iterator), \
            f'Passed parameter must be an iterator, got {type(iterable)}'

        try:
            value: int = next(iterable)
        except StopIteration:
            continue

        assert isinstance(value, int), \
            f'Values must be integer, but {value!r} its {type(value)}'

        buffer.append(
            (
                value,
                index_of_iterable
            )
        )

    last_value = None
    while buffer:
        buffer.sort(key=lambda x: x[0])
        min_value, index_of_iterable = buffer.pop(0)

        if not last_value:
            last_value = min_value
        assert last_value <= min_value, 'Iterators values arent sorted'
        last_value = min_value

        try:
            value = next(iterators[index_of_iterable])
            assert isinstance(value, int), \
                f'Values must be integer, but {value!r} its {type(value)}'

            buffer.append(
                (
                    value,
                    index_of_iterable
                )
            )
        except StopIteration:
            pass

        yield min_value
